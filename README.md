### About

This project is forked from Picard toolkit. It has modifications to read/write BAM files from HDFS, handling partial/full match read pairs in MarkDuplicates, etc.

### Building

##### Eclipse IDE
1. Import the code from `gesall-picard` repository into Eclipse.
2. Add `gesall-htsjdk` Eclipse project to dependencies in `Project->Properties->Java Build Path->Projects`.
3. Add all the external JAR files from `gesall-picard/lib` into `Project->Properties->Java Build Path->Libraries`.

##### Command line 
1. In the root directory of the project, create a softlink with name - `htsjdk` to the `gesall-htsjdk` directory.
2. Run `build.sh` present in the root directory.

### License

All copyright of Gesall code reserved by authors. This project includes code from Picard toolkit <https://github.com/broadinstitute/picard> project.
