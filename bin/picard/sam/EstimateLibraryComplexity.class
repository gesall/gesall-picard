����   2F  $picard/sam/EstimateLibraryComplexity  ,picard/sam/AbstractDuplicateFindingAlgorithm USAGE Ljava/lang/String; ConstantValue 	�Attempts to estimate library complexity from sequence of read pairs alone. Does so by sorting all reads by the first N bases (5 by default) of each read and then comparing reads with the first N bases identical to each other for duplicates.  Reads are considered to be duplicates if they match each other with no gaps and an overall mismatch rate less than or equal to MAX_DIFF_RATE (0.03 by default).

Reads of poor quality are filtered out so as to provide a more accurate estimate. The filtering removes reads with any no-calls in the first N bases or with a mean base quality lower than MIN_MEAN_QUALITY across either the first or second read.

Unpaired reads are ignored in this computation.

The algorithm attempts to detect optical duplicates separately from PCR duplicates and excludes these in the calculation of library size. Also, since there is no alignment to screen out technical reads one further filter is applied on the data.  After examining all reads a Histogram is built of [#reads in duplicate set -> #of duplicate sets]; all bins that contain exactly one duplicate set are then removed from the Histogram as outliers before library size is estimated. RuntimeVisibleAnnotations Lpicard/cmdline/Usage; INPUT Ljava/util/List; 	Signature  Ljava/util/List<Ljava/io/File;>; Lpicard/cmdline/Option; 	shortName I doc cOne or more files to combine and estimate library complexity from. Reads can be mapped or unmapped. OUTPUT Ljava/io/File; O -Output file to writes per-library metrics to. MIN_IDENTICAL_BASES0The minimum number of bases at the starts of reads that must be identical for reads to be grouped together for duplicate detection.  In effect total_reads / 4^max_id_bases reads will be compared at a time, so lower numbers will produce more accurate results but consume exponentially more memory and CPU. MAX_DIFF_RATE D IThe maximum rate of differences between two reads to call them identical. MIN_MEAN_QUALITY �The minimum mean quality of the bases in a read pair for the read to be analyzed. Reads with lower average quality are filtered out and not considered in any calculations. MAX_GROUP_RATIO �Do not process self-similar groups that are this many times over the mean expected group size. I.e. if the input contains 10m read pairs and MIN_IDENTICAL_BASES is set to 5, then the mean expected group size would be approximately 10 reads. log Lhtsjdk/samtools/util/Log; main ([Ljava/lang/String;)V Code
  ( ) * <init> ()V
  , - % instanceMainWithExit LineNumberTable LocalVariableTable args [Ljava/lang/String;
  (	  4  	  6  ?��Q��	  :  	  <  	  >   
 @ B A htsjdk/samtools/util/Log C D getInstance -(Ljava/lang/Class;)Lhtsjdk/samtools/util/Log;	  F " #
 H J I java/lang/Runtime K L 
getRuntime ()Ljava/lang/Runtime;
 H N O P 	maxMemory ()J	 R T S 7picard/sam/EstimateLibraryComplexity$PairedReadSequence U  size_in_bytes
 W Y X java/lang/Integer Z [ valueOf (I)Ljava/lang/Integer;	  ] ^ _ MAX_RECORDS_IN_RAM Ljava/lang/Integer; this &Lpicard/sam/EstimateLibraryComplexity; doWork ()I	  e   g i h java/util/List j k iterator ()Ljava/util/Iterator; m o n java/util/Iterator p q next ()Ljava/lang/Object; s java/io/File
 u w v htsjdk/samtools/util/IOUtil x y assertFileIsReadable (Ljava/io/File;)V m { | } hasNext ()Z  java/lang/Object � java/lang/StringBuilder � Will store 
 � � ) � (Ljava/lang/String;)V
 � � � � append -(Ljava/lang/Object;)Ljava/lang/StringBuilder; � % read pairs in memory before sorting.
 � � � � -(Ljava/lang/String;)Ljava/lang/StringBuilder;
 � � � � toString ()Ljava/lang/String;
 @ � � � info ([Ljava/lang/Object;)V � java/util/ArrayList
 � ( � 4picard/sam/EstimateLibraryComplexity$PairedReadCodec
 � ( � 9picard/sam/EstimateLibraryComplexity$PairedReadComparator
 � � ) � )(Lpicard/sam/EstimateLibraryComplexity;)V
 W � � c intValue	  � �  TMP_DIR
 � � � &htsjdk/samtools/util/SortingCollection � � newInstance �(Ljava/lang/Class;Lhtsjdk/samtools/util/SortingCollection$Codec;Ljava/util/Comparator;ILjava/util/Collection;)Lhtsjdk/samtools/util/SortingCollection; � #htsjdk/samtools/util/ProgressLogger B@ � Read
 � � ) � 0(Lhtsjdk/samtools/util/Log;ILjava/lang/String;)V � java/util/HashMap
 � ( � htsjdk/samtools/SAMFileReader
 � � ) y
 � � � � getFileHeader !()Lhtsjdk/samtools/SAMFileHeader;
 � � � htsjdk/samtools/SAMFileHeader � � getReadGroups ()Ljava/util/List; g � � � addAll (Ljava/util/Collection;)Z
 � i � htsjdk/samtools/SAMRecord
 � � � } getReadPairedFlag
 � � � } getFirstOfPairFlag
 � � � } getSecondOfPairFlag
 � � � � getReadName � � � java/util/Map � � remove &(Ljava/lang/Object;)Ljava/lang/Object;
 R (
  � � � addLocationInformation T(Ljava/lang/String;Lpicard/sam/AbstractDuplicateFindingAlgorithm$PhysicalLocation;)Z
 � � � � getReadGroup &()Lhtsjdk/samtools/SAMReadGroupRecord; g � � � indexOf (Ljava/lang/Object;)I
 R � � � setReadGroup (S)V � � � � put 8(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
 � � � � getReadBases ()[B
 � � � � getBaseQualities
  � �  passesQualityCheck 	([B[BII)Z	 R 	qualityOk Z
 � } getReadNegativeStrandFlag
	
 !htsjdk/samtools/util/SequenceUtil reverseComplement ([B)V	 R read1 [B	 R read2
 � add (Ljava/lang/Object;)V
 � record (Lhtsjdk/samtools/SAMRecord;)Z 8Finished reading - moving on to scanning for duplicates.  %htsjdk/samtools/util/PeekableIterator
 �" j# *()Lhtsjdk/samtools/util/CloseableIterator;
% )& (Ljava/util/Iterator;)V
(*) java/lang/System+ P currentTimeMillis@      
/10 java/lang/Math23 pow (DD)D
/567 max (II)I
 9:; getNextGroup 9(Lhtsjdk/samtools/util/PeekableIterator;)Ljava/util/List; g=> c size g@AB get (I)Ljava/lang/Object;D Omitting group with over 
 �F �G (I)Ljava/lang/StringBuilder;I / times the expected mean number of read pairs. K Mean=M 	, Actual=O . Prefixes: 
QSR htsjdk/samtools/util/StringUtilTU bytesToString ([BII)Ljava/lang/String;W  / 
 @YZ � warn
 \]^ splitByLibrary 1(Ljava/util/List;Ljava/util/List;)Ljava/util/Map; �`ab entrySet ()Ljava/util/Set;d ie java/util/Setg java/util/Map$Entryfij q getKeyl java/lang/Stringfno q getValue �qA �s htsjdk/samtools/util/Histogramu duplication_group_count
rw )x '(Ljava/lang/String;Ljava/lang/String;)Vz optical_duplicates
 |}~ matches v(Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;D)Z g�� (Ljava/lang/Object;)Z g��� set '(ILjava/lang/Object;)Ljava/lang/Object;
r��� 	increment (Ljava/lang/Comparable;)V	 ��   OPTICAL_DUPLICATE_PIXEL_DISTANCE
 ��� findOpticalDuplicates (Ljava/util/List;I)[Z      �`� 
Processed �  groups.
 {
�� * close
 ��� * cleanup
 ��� getMetricsFile '()Lhtsjdk/samtools/metrics/MetricsFile; ���b keySet� picard/sam/DuplicationMetrics
� (	���  LIBRARY
r�
rq� "htsjdk/samtools/util/Histogram$Bin
��o� ()D	���� READ_PAIRS_EXAMINED J	���� READ_PAIR_DUPLICATES	���� READ_PAIR_OPTICAL_DUPLICATES
��� * calculateDerivedMetrics
��� #htsjdk/samtools/metrics/MetricsFile�� 	addMetric '(Lhtsjdk/samtools/metrics/MetricBase;)V
���� addHistogram #(Lhtsjdk/samtools/util/Histogram;)V	 �  
��� y write f 
readGroups recordsRead sorter (Lhtsjdk/samtools/util/SortingCollection; progress %Lhtsjdk/samtools/util/ProgressLogger; pendingByName Ljava/util/Map; in Lhtsjdk/samtools/SAMFileReader; rec Lhtsjdk/samtools/SAMRecord; prs 9Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence; rg $Lhtsjdk/samtools/SAMReadGroupRecord; bases 'Lhtsjdk/samtools/util/PeekableIterator; duplicationHistosByLibrary opticalHistosByLibrary groupsProcessed lastLogTime meanGroupSize group sequencesByLibrary entry Ljava/util/Map$Entry; library seqs duplicationHisto  Lhtsjdk/samtools/util/Histogram; opticalHisto i lhs dupes j rhs duplicateCount flags [Z b file %Lhtsjdk/samtools/metrics/MetricsFile; metrics Lpicard/sam/DuplicationMetrics; bin duplicateGroups opticalDuplicates LocalVariableTypeTable 6Ljava/util/List<Lhtsjdk/samtools/SAMReadGroupRecord;>; cLhtsjdk/samtools/util/SortingCollection<Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;>; \Ljava/util/Map<Ljava/lang/String;Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;>; bLhtsjdk/samtools/util/PeekableIterator<Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;>; XLjava/util/Map<Ljava/lang/String;Lhtsjdk/samtools/util/Histogram<Ljava/lang/Integer;>;>; KLjava/util/List<Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;>; nLjava/util/Map<Ljava/lang/String;Ljava/util/List<Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;>;>; tLjava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;>;>; 5Lhtsjdk/samtools/util/Histogram<Ljava/lang/Integer;>; YLhtsjdk/samtools/metrics/MetricsFile<Lpicard/sam/DuplicationMetrics;Ljava/lang/Integer;>; StackMapTable�
/7 min
/ floor (D)D maxDiffRate read1Length read2Length 	maxErrors errors �(Lhtsjdk/samtools/util/PeekableIterator<Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;>;)Ljava/util/List<Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;>;
 o
 q peek first �(Ljava/util/List<Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;>;Ljava/util/List<Lhtsjdk/samtools/SAMReadGroupRecord;>;)Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lpicard/sam/EstimateLibraryComplexity$PairedReadSequence;>;>;
 R! �" ()S$ "htsjdk/samtools/SAMReadGroupRecord
#&' � 
getLibrary) Unknown input rgs out seq librarySeqs
	012 isNoCall (B)Z quals 
seedLength 
minQuality total B 
SourceFile EstimateLibraryComplexity.java InnerClasses Bin= ,htsjdk/samtools/util/SortingCollection$Codec Codec EntryA =picard/sam/AbstractDuplicateFindingAlgorithm$PhysicalLocation PhysicalLocation PairedReadCodec PairedReadComparator PairedReadSequence !               
                 
       s  s      
       s  s      
       s      
       s      
       s       
       s !  " #    	 $ %  &   :     � Y� '*� +�    .   
    �  � /        0 1    ) *  &   �     B*� 2*� 3*� 5* 7� 9*� ;*�� =*� ?� E*� G� M� Q�m�l� V� \�    .   * 
   �  7  8 
 S  V  Z  _ # a , � A � /       B ` a    b c  &  �    �*� d� f M� ,� l � rL+� t,� z ���*� E� ~Y� �Y�� �*� \� ��� �� �S� �� �Y� �L=R� �Y� �� �Y*� �*� \� �*� �� �N� �Y*� E��� �:*� d� f :�H� l � r:� �Y� �:� �Y� �:+� �� ¹ � W� �:
�
� l � �:		� Ϛ � �	� Қ 	� ՚ � �	� ع � � R:� C� RY� �:*	� �� � 	� �:� +� � �� �	� �� � W*	� �	� �*� 5*� ;� �6�� � � �	� �:	�� �	� ҙ �� 
��� �� �� 	-�	�W
� z ���� z ���*� E� ~YS� ��Y-�!�$:� �Y� �:� �Y� �:6�'7	l,*� 5h��.�l�46�g*�8:�< *� =h� ��? � R:*� E� ~Y� �YC� �*� =�EH� �J� ��EL� ��< �EN� ��*� 5�P� �V� ��*� 5�P� �� �S�X��*+�[:�_ �c :�p� l �f:�h �k:�m � g:�p �r:�p �r:� 8�rYt�v:�rYty�v:� � W� � W6� ��? � R:� � ˻ �Y� �:`6� @�? � R:� � '**� 9�{� � W�� W��< ����< � ]� W�< 6� V��**����:Y:�66� 36� � V������ � V����< ��� z ����	�'�e�� .*� E� ~Y� �Y�� ��E�� �� �S� ��'7	�������-��*��:�� �c :� �� l �k:�p �r:�p �r:��Y��:�����c :� |� l � W:������9��� � ������9�� =Y���� ��kc���Y���� �d�kc���Y���c���� z ���������� z ��*�ɶ��    .  � z   � $ � J � R � T � V � ] � e � l � p � t � � � � � � � � � � � � � � � � � � � #*=LRW[_d{��������!� �� ��%�( *	+-./2153=5N6[7|8�9�:�;�7�<�>�A�B�CEF'G,H:IIJUKaOgPuQ}R�T�U�V�X�Y�Z�T�^�_�`�a�cd e/d9g<iEOTA^naonp�q�1�v�w�y�z�{�|�}�~���#�@�G�\�s����������z���� /  � ,  � ` a    �   Ra�   T_�   t?��  �.��  �9�   �0��  �%��  � ��� 	 ��� * �� d j � � L�  � j� 	��� ��� ��  ��� 	2��  =\�  [ o�� ���� �a�� �U�  I�  ;�� '-�� d ��  u ��� � ��  � L�  � ,�� � D�   .��   � ��� � ��  � ��� � ��� � ���  m� _ # ^�  @ A�  �   �  Ra�   t?�  �0�  � j 	�� �� =\� ��� �a� I� ;� '-� � �� ��	 � �� � �� 
  � (�     m  � w   g � �  m  � 8   g � � r m � �  m  �    g � � r m � � � m  � F R� *   g � � r m � � � m R  R�     g � � r m � � � m R  R� �    g � � r m � �  m  � 	   g � �  m  � \   g � � � �  � � g�    g � � � � g �  m  � y   g � � � � g �f mk grr  � �  R�  g�  R� #� L   g � � � � g �f mk grr R g   � 	   g � � � � g �f mk grr R g  � �    g � � � � g �  m  � D   g � � � �  � '   g � � � ��  m  � F   g � � � ��k mrr�  m  � (   g � � � ��k mrr� W m  L� B   g � � � ��k mrr�  m  �    g � � � ��  m   }~  &  h  
   �+��,���6+��,���6`�)k��66*� 56	� #+�	3,�	3� �� ��		���*� 56	� #+�	3,�	3� �� ��		����    .   6   � � � ,� /� 8� I� U� _� h� y� �� �� /   f 
   � ` a     ���    ���    �    �    s   , e   / b   5 *�  	 e *�  	
    � 8 	  R R    :;      &  2     t� �Y� �M+�� RN,-� W� S+�� R:6� --�3�3� 9-�3�3� � $�*� 5���,+�� R� W+�����,�    .   .   � � � � � $� *� Q� ]� k� r� /   >    t ` a     t j�   l�    d�  $ G p�  ' 6�  �       t j   l� 
    �  g R�  R&�   ]^      &  �     �� �Y� �N+� f :� p� l � R::� � #,� �? �#�%:� (:� (:-�p � g:� � �Y� �:-� � W� W� z ���-�    .   >   � � � "� +� >� H� K� P� ]� b� k� v� �� �� /   H    � ` a     �*     �+    �,�   a-�  " ^�   ] #.  �   *    �*    �+    �,  ] #. 
   S �    g g �  m  � 7   g g � R mk  � % g� 	   g g �  m     �   &    
   Y+�� �6� +3�/� �����6,Y:	�66� 	36`6����,�l� ��    .      � � � � #� &� K� /   R    Y ` a     Y�    Y3    Y4     Y5    �   & 36   : �7 
   6 � �  
    �      8   9:   : �r; < �>	f �?	@ B	 � C  � D   R E 